import sys
import socket

UDP_IP = "127.0.0.1"
UDP_PORT = 4876
if len(sys.argv) > 2:
    MESSAGE = bytes.fromhex(sys.argv[2])
else:
    MESSAGE = bytes.fromhex("3c6f3c653c603c6b")

print("UDP target IP: %s" % UDP_IP)
print("UDP target port: %s" % UDP_PORT)
print("message: %s" % MESSAGE)

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))
