# This Repo Has Moved

This repo has moved to: https://github.com/team-arrow-racing/mission-control

# Vehicle Obervability Pipeline

This is the Team Arrow vehicle observability pipeline.

## Setup

Run the following command to spin-up the telemetry stack.

```shell
docker-compose up
```

## Agent Goals
* Recieve and process CAN bus packets.
* Transform raw data into useful metrics using DBC files.
* Step-through CAN packets like a debugger.
* Lightweight.
    * It should be possible to run this software on a macbook for a day without running out of charge.
    * Startup times should be measured in seconds, not minutes.
* Non-blocking. (shouldn't miss messages)

## Feature checklist
- [x] Recieve CAN packets over UDP multicast
- [ ] Decode CAN packets
- [ ] Store CAN packets
- [ ] Generate metrics based on CAN packets
- [ ] Frontend to view metrics and packets (live)
- [ ] Frontend to step through metrics and packets (historical)
- [ ] Realtime alerting rules

## Architecture

### Components
* [Grafana](https://grafana.com/grafana/) - data visualisation.
* [Loki](https://grafana.com/oss/loki/) - log aggregation.
* [Mimir](https://grafana.com/oss/mimir/) - metrics aggregation.
* Custom agent - reads and processes CAN bus frames.

### Data flow
![Telemetry Pipeline](telemetry-pipeline.drawio.svg)

# About Team Arrow
Team Arrow Racing Association is a volunteer organisation that designs, develops and races world-class solar powered vehicles. This repository is part of our endevour to build in the open and make our learnings accessible to anyone.
You can find our more about Team Arrow on our website.
