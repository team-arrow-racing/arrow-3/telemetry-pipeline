# Telemetry Agent

## Running the agent

```shell
cargo run
```

## Building the docker image

```shell
docker build -t telemetry-agent .
```

## Running the docker image

```shell
docker run -it telemetry-agent
```