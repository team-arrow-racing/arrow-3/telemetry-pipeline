#![allow(dead_code)]

use bitflags::bitflags;
use socketcan::CANFrame;
use std::io;
use std::net::UdpSocket;

struct Frame {
    version_identifier: u64,
    bus_number: u8,
    client_identifier: u64,
    identifier: u32,
    flags: Flags,
    length: u8,
    data: [u8],
}

bitflags! {
    #[derive(Debug, Copy, Clone, PartialEq, Eq)]
    struct Flags: u8 {
        const HEARTBEAT = (1 << 0);
        const SETTINGS = (1 << 1);
        const RTR = (1 << 6);
        const EXTENDED = (1 << 7);
    }
}

/// Tritium CAN-Ethernet bridge
pub struct Bridge {
    socket: UdpSocket,
}

impl Bridge {
    /// Create a new bridge instance
    pub fn new(socket: UdpSocket) -> io::Result<Self> {
        Ok(Self { socket })
    }

    /// Send can frame
    pub fn send(&mut self, _frame: CANFrame) {}

    /// Receive can frame
    pub fn recv_from(&mut self) -> io::Result<CANFrame> {
        let mut buf = [0; 30];

        match self.socket.recv_from(&mut buf) {
            Ok((_, _)) => {
                // not used, but might be later
                let _bus_id = &buf[1..8];
                let _client_id = &buf[9..16];

                let id = u32::from_be_bytes(buf[16..20].try_into().unwrap());

                let flags = Flags::from_bits(buf[20]).unwrap();

                let rtr = flags.contains(Flags::RTR);
                let length: usize = buf[21] as usize;
                let data: &[u8] = &buf[22..(22 + length)];

                Ok(CANFrame::new(id, data, rtr, false).unwrap())
            }
            Err(e) => {
                eprintln!("Error receiving data: {}", e);
                Err(e)
            }
        }
    }
}
