use loki_client::{Builder, Loki, Streams};
use socketcan::{CANFrame, CANSocket};
extern crate tokio;

#[tokio::main]
async fn main() {
    let loki = Loki::new("http://localhost:3100");

    let can = match CANSocket::open("can0") {
        Ok(socket) => socket,
        Err(e) => {
            panic!("could not open socket: {e}");
        }
    };

    loop {
        match can.read_frame() {
            Ok(frame) => {
                let stream = Builder::new()
                    .label("interface", "can0")
                    .log(None, fmt_candump(&"can0".to_string(), &frame))
                    .build();

                let streams = Streams {
                    streams: vec![stream]
                };

                loki.push(streams).await.unwrap();
            },
            Err(e) => {
                log::error!("Failed to read can frame");
                continue;
            }
        }
    }
}

pub fn fmt_candump(interface: &String, frame: &CANFrame) -> String {
    let mut result = format!("{}  {:X}  [{}] ", interface, frame.id(), frame.data().len());

    for byte in frame.data().iter() {
        result = format!("{result} {byte:02X}");
    }

    result
}
